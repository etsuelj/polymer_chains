"""
Code for Polymer Chains (W.Kinzel/G.Reents, Physics by Computer)
"""

from __future__ import division
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation

cl = 10 # chain length
N = int(5*cl) # linear lattice dimension
num_config = 100 # number of reptation steps  


def gen_init(grid = np.zeros([N,N],dtype=int),chains = []):
    '''
    Generate a random initial configuration of the polymer chain.
    '''
    hx,hy = N//2,N//2 # start at the middle of the square lattice
    grid[hy,hx] = 2 # head
    chains.append((hy,hx))    

    for i in xrange(cl-1):
        next_chain = [(0,1),(1,0),(-1,0),(0,-1)] # up,left,right,down
        step = np.random.choice(len(next_chain))
        hxx,hyy = next_chain[step][0]+hx,next_chain[step][1]+hy
        while hxx<0 or hyy<0 or hxx>N-1 or hyy>N-1 or grid[hyy,hxx]==1 or grid[hyy,hxx]==2:
            next_chain = np.delete(next_chain,step,axis=0)
            if len(next_chain)==0:
                raise ValueError('Oh no, chain got stuck! Try again.') 
                gen_init(grid = np.zeros([N,N],dtype=int),chains = [])
            step = np.random.choice(len(next_chain))
            hxx,hyy = next_chain[step][0]+hx,next_chain[step][1]+hy
        if i==cl-2:
            grid[hyy,hxx] = 3 # tail      
        else:
            grid[hyy,hxx] = 1 
        hx,hy = hxx,hyy
        chains.append((hy,hx))
    return grid,chains
 
    
def reptation(grid,chains,N):
    '''Steps 2 and 3 of reptation process'''
    
    #randomly select one of the sites adjacent to the head
    next_chain = [(0,1),(1,0),(-1,0),(0,-1)] # up,left,right,down
    step = np.random.choice(len(next_chain))
    new_head = (chains[0][0]+next_chain[step][1],chains[0][1]+next_chain[step][0])    
    if grid[new_head]!=0: # occupied
        grid[chains[0]],grid[chains[-1]] = 3,2 # swap head and tail
        chains = chains[::-1]
    else:
        grid[chains[-1]] = 0    # remove tail end
        grid[new_head] = 2   # add a new head monomer
        grid[chains[0]] = 1
        chains.insert(0,new_head)
        chains.remove(chains[-1])
        grid[chains[-1]] = 3  # set new tail
    return grid,chains
    
def distance(chains):
    '''Calculate displacement between head and tail.'''
    x1,y1 = chains[0][1],chains[0][0]
    x2,y2 = chains[-1][1],chains[-1][0]
    return np.sqrt((x2-x1)**2+(y2-y1)**2)

def continue_loop():
    '''
    Create a generator.
    '''
    i = 0
    while i < num_config:
        i += 1
        yield i
        
      
def update(continue_loop,grid,chains,line,mga_distance,title,scatter):
    mga_distance.append(distance(chains))
    grid,chains = reptation(grid,chains,N)
    if continue_loop<num_config:
        label = 'chain length=%i \n end-to-end distance=%.3f \n number of iterations: %i out of %i'%(cl,distance(chains),continue_loop,num_config)
    else:
        mean_dist = np.mean(mga_distance) # average end-to-end distance
        label = 'chain length=%i \n <end-to-end distance>=%.3f \n %ith iteration #END'%(cl,mean_dist,continue_loop)
    line.set_data(zip(*chains)[0],zip(*chains)[1])
    title.set_text('head:red, tail:black \n'+label)
    scatter.set_offsets(chains[0])
    return line,title,scatter,mga_distance
    
grid,chains=gen_init()
mga_distance = [] # will store end-to-end distance for each reptation step
        
print('Result involves animation. Use Python console or VIDLE instead of IPython console\
 if animation is not shown. There would be times when the chain would get stuck before the iteration\
 finishes; in that case, run the program again.')
fig = plt.figure()
line, = plt.plot([], [],'c-',linewidth=2,zorder=1)
scatter = plt.scatter([],[],color='r',clip_on=0,s=20,zorder=2)
title = plt.title('')
plt.ylim(0,N)
plt.xlim(0,N)
anim = FuncAnimation(fig,update,continue_loop,
                     fargs=(grid,chains,line,mga_distance,title,scatter),
                        interval=10, repeat=0)
plt.tight_layout()
plt.show()